# Frontend Test

Hi %Username%

In this test task you will need to Create 2 small projects.
For simplifying the process, you should do them in 1 repository.
Create a fork of this repository and create a merge request at the end of your work

## Autocomplete Package

First of all you have to create an Autocomplete Library using native JS or typescript.
You cannot use any library and you must not copy any code from any library.
Except webpack.

Below you'll find all the issues/features you should work on.
Some of them are required, some of them are optional.
You can not to do any optional features, but solution of them is a big plus.
Anyway code with better quality is preferred more than amount of issues.

Basic functionality:
- [X] Autocomplete Input
- [X] Container for results
- [X] When you type `n` letters you'll need to wait `m` ms before you call the search
- [X] Ability to provide source callback, which will return to you an array of JSON objects
- [X] Ability to provide several source callbacks and use them one by one until the result of callback return not empty array. (OPTIONAL)
- [X] Add accessibility for screen reader (ARIA) (OPTIONAL)
- [X] Add ability to pass a render callback. this callback should receive the result from source
- [X] Add ability to pass a render callbacks, which should be chosen depending on the source callback, which would return not empty results (OPTIONAL)
- [X] Add ability to use keyboard (OPTIONAL)
- [X] Add ability to pass custom css for input/container/etc
- [X] Loading Spinner
- [X] Unit tests coverage (OPTIONAL)
- [X] Minifying with webpack (OPTIONAL)


## Analytics webpage

Secondly you'll need to create a simple webpage layout using:
- node.js
- React.js
- autocomplete package you'd created
- any other libraries you need

Functionality of this module is pretty simple.
[X] Header/Footer/etc just for some pretty UI
There will be 2 blocks, which should be in a row if possible or in a column if not (simple responsiveness).
- [X] First block - integrate autocomplete and wrap it in a ReactJS Component, which will give an ability to search city.
When you press the city, then the second block appears and should be in focus.
- [X] Second block - is a section with Chart of amounts of data (population/cost of living, etc)/time.
For charts you can use any library you want.

In folder fixtures you'll find
- cities.json with mocked data of cities for autocomplete.
- data folder, which contains files with detailed data of characteristics of cities.

### Analytics app using React Native (OPTIONAL)

When you finish your webpage, if you want to try/work with react native.
You can create the application with the same functionality as in webpage.
It's very optional. We understand, it takes time. But is a huge plus.


## PS

Please go feature by feature. And please add prefixes of features you work on in a header of your commit messages.
Background description in commit messages is a plus. Mark completed issues in this file, please.
