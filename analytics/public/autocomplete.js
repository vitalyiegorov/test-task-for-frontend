/******/ (function(modules) { // webpackBootstrap
    /******/ 	// The module cache
    /******/ 	var installedModules = {};
    /******/
    /******/ 	// The require function
    /******/ 	function __webpack_require__(moduleId) {
        /******/
        /******/ 		// Check if module is in cache
        /******/ 		if(installedModules[moduleId]) {
            /******/ 			return installedModules[moduleId].exports;
            /******/ 		}
        /******/ 		// Create a new module (and put it into the cache)
        /******/ 		var module = installedModules[moduleId] = {
            /******/ 			i: moduleId,
            /******/ 			l: false,
            /******/ 			exports: {}
            /******/ 		};
        /******/
        /******/ 		// Execute the module function
        /******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
        /******/
        /******/ 		// Flag the module as loaded
        /******/ 		module.l = true;
        /******/
        /******/ 		// Return the exports of the module
        /******/ 		return module.exports;
        /******/ 	}
    /******/
    /******/
    /******/ 	// expose the modules object (__webpack_modules__)
    /******/ 	__webpack_require__.m = modules;
    /******/
    /******/ 	// expose the module cache
    /******/ 	__webpack_require__.c = installedModules;
    /******/
    /******/ 	// define getter function for harmony exports
    /******/ 	__webpack_require__.d = function(exports, name, getter) {
        /******/ 		if(!__webpack_require__.o(exports, name)) {
            /******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
            /******/ 		}
        /******/ 	};
    /******/
    /******/ 	// define __esModule on exports
    /******/ 	__webpack_require__.r = function(exports) {
        /******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
            /******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
            /******/ 		}
        /******/ 		Object.defineProperty(exports, '__esModule', { value: true });
        /******/ 	};
    /******/
    /******/ 	// create a fake namespace object
    /******/ 	// mode & 1: value is a module id, require it
    /******/ 	// mode & 2: merge all properties of value into the ns
    /******/ 	// mode & 4: return value when already ns object
    /******/ 	// mode & 8|1: behave like require
    /******/ 	__webpack_require__.t = function(value, mode) {
        /******/ 		if(mode & 1) value = __webpack_require__(value);
        /******/ 		if(mode & 8) return value;
        /******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
        /******/ 		var ns = Object.create(null);
        /******/ 		__webpack_require__.r(ns);
        /******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
        /******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
        /******/ 		return ns;
        /******/ 	};
    /******/
    /******/ 	// getDefaultExport function for compatibility with non-harmony modules
    /******/ 	__webpack_require__.n = function(module) {
        /******/ 		var getter = module && module.__esModule ?
            /******/ 			function getDefault() { return module['default']; } :
            /******/ 			function getModuleExports() { return module; };
        /******/ 		__webpack_require__.d(getter, 'a', getter);
        /******/ 		return getter;
        /******/ 	};
    /******/
    /******/ 	// Object.prototype.hasOwnProperty.call
    /******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
    /******/
    /******/ 	// __webpack_public_path__
    /******/ 	__webpack_require__.p = "";
    /******/
    /******/
    /******/ 	// Load entry module and return exports
    /******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.ts");
    /******/ })
/************************************************************************/
/******/ ({

    /***/ "./src/autocomplete-options.interface.ts":
    /*!***********************************************!*\
      !*** ./src/autocomplete-options.interface.ts ***!
      \***********************************************/
    /*! no static exports found */
    /***/ (function(module, exports, __webpack_require__) {

        "use strict";

        Object.defineProperty(exports, "__esModule", { value: true });
        exports.defaultAutocompleteOptions = {
            callbacks: [],
            inputClasses: [],
            resultsClasses: [],
            resultsItemClasses: [],
            containerClasses: [],
            inputTimeoutDelay: 500
        };


        /***/ }),

    /***/ "./src/autocomplete.ts":
    /*!*****************************!*\
      !*** ./src/autocomplete.ts ***!
      \*****************************/
    /*! no static exports found */
    /***/ (function(module, exports, __webpack_require__) {

        "use strict";

        var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
            return new (P || (P = Promise))(function (resolve, reject) {
                function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
                function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
                function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
                step((generator = generator.apply(thisArg, _arguments || [])).next());
            });
        };
        var __generator = (this && this.__generator) || function (thisArg, body) {
            var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
            return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
            function verb(n) { return function (v) { return step([n, v]); }; }
            function step(op) {
                if (f) throw new TypeError("Generator is already executing.");
                while (_) try {
                    if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                    if (y = 0, t) op = [op[0] & 2, t.value];
                    switch (op[0]) {
                        case 0: case 1: t = op; break;
                        case 4: _.label++; return { value: op[1], done: false };
                        case 5: _.label++; y = op[1]; op = [0]; continue;
                        case 7: op = _.ops.pop(); _.trys.pop(); continue;
                        default:
                            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                            if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                            if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                            if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                            if (t[2]) _.ops.pop();
                            _.trys.pop(); continue;
                    }
                    op = body.call(thisArg, _);
                } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
                if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
            }
        };
        Object.defineProperty(exports, "__esModule", { value: true });
        var autocomplete_options_interface_1 = __webpack_require__(/*! ./autocomplete-options.interface */ "./src/autocomplete-options.interface.ts");
        var styles = __webpack_require__(/*! ../style/autocomplete.scss */ "./style/autocomplete.scss");
        var AutoComplete = /** @class */ (function () {
            function AutoComplete() {
                this.CSS_PARENT_CLASS = 'cm-ac';
                this.CSS_LOADING_CLASS = 'cm-ac__loading';
                this.CSS_CONTAINER_CLASS = 'cm-ac-container';
                this.CSS_INPUT_CLASS = 'cm-ac-input';
                this.CSS_RESULTS_CLASS = 'cm-ac-results';
                this.CSS_RESULTS_HIDDEN_CLASS = this.CSS_RESULTS_CLASS + '__hidden';
                this.CSS_RESULTS_ITEM_CLASS = this.CSS_RESULTS_CLASS + '--item';
                this.CSS_RESULTS_ITEM_ACTIVE_CLASS = this.CSS_RESULTS_ITEM_CLASS + '__active';
                this.INPUT_TIMEOUT = 500;
                this.callbacks = [];
                this.inputClasses = [this.CSS_INPUT_CLASS];
                this.containerClasses = [this.CSS_CONTAINER_CLASS];
                this.resultsClasses = [this.CSS_RESULTS_CLASS, this.CSS_RESULTS_HIDDEN_CLASS];
                this.resultsItemClasses = [this.CSS_RESULTS_ITEM_CLASS];
                this.previousInput = '';
                this.isLoading = false;
                this.domCreated = false;
                this.currentFocusIdx = -1;
                this.inputTimeoutDelay = this.INPUT_TIMEOUT;
                this.suggestions = [];
            }
            AutoComplete.prototype.init = function (parent, _a, selectCb) {
                var _b = _a === void 0 ? autocomplete_options_interface_1.defaultAutocompleteOptions : _a, callbacks = _b.callbacks, inputClasses = _b.inputClasses, resultsClasses = _b.resultsClasses, resultsItemClasses = _b.resultsItemClasses, containerClasses = _b.containerClasses, inputTimeoutDelay = _b.inputTimeoutDelay;
                if (selectCb === void 0) { selectCb = function () { }; }
                this.appendArray(this.callbacks, callbacks);
                this.appendArray(this.inputClasses, inputClasses);
                this.appendArray(this.resultsClasses, resultsClasses);
                this.appendArray(this.resultsItemClasses, resultsItemClasses);
                this.appendArray(this.containerClasses, containerClasses);
                this.inputTimeoutDelay = inputTimeoutDelay ? inputTimeoutDelay : this.INPUT_TIMEOUT;
                this.selectCb = selectCb;
                this.create(parent);
            };
            AutoComplete.prototype.create = function (parent) {
                if (!this.domCreated) {
                    parent.classList.add(this.CSS_PARENT_CLASS);
                    this.domContainer = document.createElement('div');
                    this.applyStyles(this.domContainer, this.containerClasses);
                    parent.appendChild(this.domContainer);
                    this.domInput = document.createElement('input');
                    this.applyStyles(this.domInput, this.inputClasses);
                    this.domInput.addEventListener('keyup', this.onKeyUp.bind(this));
                    this.domInput.addEventListener('keydown', this.onKeyDown.bind(this));
                    this.domInput.setAttribute('aria-label', 'Test label');
                    this.domInput.setAttribute('aria-required', 'true');
                    this.domContainer.appendChild(this.domInput);
                    this.domResults = document.createElement('div');
                    this.applyStyles(this.domResults, this.resultsClasses);
                    this.domContainer.appendChild(this.domResults);
                    this.domCreated = true;
                }
            };
            AutoComplete.prototype.onKeyUp = function () {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, this.handleInput(this.domInput.value)];
                            case 1:
                                _a.sent();
                                return [2 /*return*/];
                        }
                    });
                });
            };
            AutoComplete.prototype.onKeyDown = function (event) {
                return __awaiter(this, void 0, void 0, function () {
                    var i;
                    return __generator(this, function (_a) {
                        switch (event.code) {
                            case "ArrowDown":
                                this.currentFocusIdx = this.currentFocusIdx < this.suggestions.length - 1 ? this.currentFocusIdx + 1 : 0;
                                event.preventDefault();
                                break;
                            case "ArrowUp":
                                this.currentFocusIdx = this.currentFocusIdx > 0 ? this.currentFocusIdx - 1 : this.suggestions.length - 1;
                                event.preventDefault();
                                break;
                            case "Enter":
                                if (this.currentFocusIdx >= 0 && this.currentFocusIdx < this.suggestions.length) {
                                    this.selectResult(this.domResults.children[this.currentFocusIdx].innerHTML);
                                }
                                break;
                        }
                        for (i = 0; i < this.domResults.children.length; i++) {
                            this.domResults.children[i].classList.remove(this.CSS_RESULTS_ITEM_ACTIVE_CLASS);
                            if (i === this.currentFocusIdx) {
                                this.domResults.children[i].classList.add(this.CSS_RESULTS_ITEM_ACTIVE_CLASS);
                            }
                        }
                        return [2 /*return*/];
                    });
                });
            };
            AutoComplete.prototype.update = function (input) {
                if (input === void 0) { input = ''; }
                return __awaiter(this, void 0, void 0, function () {
                    var _i, _a, _b, source, _c, render, foundMatch, pattern, _d, _e, suggestion;
                    return __generator(this, function (_f) {
                        switch (_f.label) {
                            case 0:
                                this.clearResults();
                                _i = 0, _a = this.callbacks;
                                _f.label = 1;
                            case 1:
                                if (!(_i < _a.length)) return [3 /*break*/, 7];
                                _b = _a[_i], source = _b.source, _c = _b.render, render = _c === void 0 ? function (value) { return value; } : _c;
                                foundMatch = false;
                                pattern = new RegExp("^" + input, "i");
                                _d = 0;
                                return [4 /*yield*/, source(input)];
                            case 2:
                                _e = (_f.sent()).map(render);
                                _f.label = 3;
                            case 3:
                                if (!(_d < _e.length)) return [3 /*break*/, 5];
                                suggestion = _e[_d];
                                if (pattern.test(suggestion)) {
                                    this.renderResultsItem(suggestion);
                                    this.suggestions.push(suggestion);
                                    foundMatch = true;
                                }
                                _f.label = 4;
                            case 4:
                                _d++;
                                return [3 /*break*/, 3];
                            case 5:
                                if (foundMatch) {
                                    this.showResults();
                                    return [3 /*break*/, 7];
                                }
                                _f.label = 6;
                            case 6:
                                _i++;
                                return [3 /*break*/, 1];
                            case 7: return [2 /*return*/];
                        }
                    });
                });
            };
            AutoComplete.prototype.selectResult = function (selectedValue) {
                this.domInput.value = selectedValue;
                this.clearResults();
                this.selectCb(selectedValue);
            };
            AutoComplete.prototype.handleInput = function (input) {
                return __awaiter(this, void 0, void 0, function () {
                    var _this = this;
                    return __generator(this, function (_a) {
                        clearTimeout(this.inputTimeout);
                        this.inputTimeout = setTimeout(function () { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        console.log('Updaing');
                                        if (!this.isUpdateNeeded(input)) return [3 /*break*/, 2];
                                        this.previousInput = input;
                                        this.showLoading();
                                        return [4 /*yield*/, this.update(input)];
                                    case 1:
                                        _a.sent();
                                        this.hideLoading();
                                        return [3 /*break*/, 3];
                                    case 2:
                                        if (input.length === 0) {
                                            this.clearResults();
                                        }
                                        _a.label = 3;
                                    case 3: return [2 /*return*/];
                                }
                            });
                        }); }, this.inputTimeoutDelay);
                        return [2 /*return*/];
                    });
                });
            };
            AutoComplete.prototype.showLoading = function () {
                this.isLoading = true;
                this.toggleClass(this.domContainer, this.CSS_LOADING_CLASS);
            };
            AutoComplete.prototype.hideLoading = function () {
                this.isLoading = false;
                this.toggleClass(this.domContainer, this.CSS_LOADING_CLASS);
            };
            AutoComplete.prototype.isUpdateNeeded = function (input) {
                return input !== this.previousInput && input.length > 0;
            };
            AutoComplete.prototype.clearResults = function () {
                this.suggestions = [];
                this.domResults.innerHTML = '';
                this.domResults.classList.add(this.CSS_RESULTS_HIDDEN_CLASS);
            };
            AutoComplete.prototype.showResults = function () {
                this.domResults.classList.remove(this.CSS_RESULTS_HIDDEN_CLASS);
            };
            AutoComplete.prototype.renderResultsItem = function (suggestion) {
                var _this = this;
                var _a;
                var domSuggestion = document.createElement('div');
                domSuggestion.appendChild(document.createTextNode(suggestion));
                (_a = domSuggestion.classList).add.apply(_a, this.resultsItemClasses);
                domSuggestion.addEventListener('click', function () { return _this.selectResult(suggestion); });
                this.domResults.appendChild(domSuggestion);
            };
            AutoComplete.prototype.toggleClass = function (dom, className) {
                if (dom.classList.contains(className)) {
                    dom.classList.remove(className);
                }
                else {
                    dom.classList.add(className);
                }
            };
            AutoComplete.prototype.applyStyles = function (dom, styles) {
                var _a;
                (_a = dom.classList).add.apply(_a, styles.filter(function (css) { return css.length; }));
            };
            AutoComplete.prototype.appendArray = function (source, array) {
                if (array === void 0) { array = []; }
                source.push.apply(source, array);
                return source;
            };
            return AutoComplete;
        }());
        exports.AutoComplete = AutoComplete;


        /***/ }),

    /***/ "./src/index.ts":
    /*!**********************!*\
      !*** ./src/index.ts ***!
      \**********************/
    /*! no static exports found */
    /***/ (function(module, exports, __webpack_require__) {

        "use strict";

        Object.defineProperty(exports, "__esModule", { value: true });
        var autocomplete_1 = __webpack_require__(/*! ./autocomplete */ "./src/autocomplete.ts");
        (function () {
            window['autoComplete'] = function (parent, opts, cb) {
                var ac = new autocomplete_1.AutoComplete();
                ac.init(parent, opts, cb);
                return ac;
            };
        })();


        /***/ }),

    /***/ "./style/autocomplete.scss":
    /*!*********************************!*\
      !*** ./style/autocomplete.scss ***!
      \*********************************/
    /*! no static exports found */
    /***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

        /***/ })

    /******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2F1dG9jb21wbGV0ZS1vcHRpb25zLmludGVyZmFjZS50cyIsIndlYnBhY2s6Ly8vLi9zcmMvYXV0b2NvbXBsZXRlLnRzIiwid2VicGFjazovLy8uL3NyYy9pbmRleC50cyIsIndlYnBhY2s6Ly8vLi9zdHlsZS9hdXRvY29tcGxldGUuc2Nzcz80MmYzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtEQUEwQyxnQ0FBZ0M7QUFDMUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnRUFBd0Qsa0JBQWtCO0FBQzFFO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUF5QyxpQ0FBaUM7QUFDMUUsd0hBQWdILG1CQUFtQixFQUFFO0FBQ3JJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7OztBQ3ZFYSxrQ0FBMEIsR0FBRztJQUN4QyxTQUFTLEVBQUUsRUFBRTtJQUNiLFlBQVksRUFBRSxFQUFFO0lBQ2hCLGNBQWMsRUFBRSxFQUFFO0lBQ2xCLGtCQUFrQixFQUFFLEVBQUU7SUFDdEIsZ0JBQWdCLEVBQUUsRUFBRTtJQUNwQixpQkFBaUIsRUFBRSxHQUFHO0NBQ3ZCLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDakJGLDhJQUEwRztBQUUxRyxJQUFNLE1BQU0sR0FBRyxtQkFBTyxDQUFDLDZEQUE0QixDQUFDLENBQUM7QUFFckQ7SUFBQTtRQUNXLHFCQUFnQixHQUFHLE9BQU8sQ0FBQztRQUMzQixzQkFBaUIsR0FBRyxnQkFBZ0IsQ0FBQztRQUNyQyx3QkFBbUIsR0FBRyxpQkFBaUIsQ0FBQztRQUN4QyxvQkFBZSxHQUFHLGFBQWEsQ0FBQztRQUNoQyxzQkFBaUIsR0FBRyxlQUFlLENBQUM7UUFDcEMsNkJBQXdCLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixHQUFHLFVBQVUsQ0FBQztRQUMvRCwyQkFBc0IsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsUUFBUSxDQUFDO1FBQzNELGtDQUE2QixHQUFHLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxVQUFVLENBQUM7UUFDekUsa0JBQWEsR0FBRyxHQUFHLENBQUM7UUFFckIsY0FBUyxHQUF3QixFQUFFLENBQUM7UUFDcEMsaUJBQVksR0FBYSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUNoRCxxQkFBZ0IsR0FBYSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBQ3hELG1CQUFjLEdBQWEsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLHdCQUF3QixDQUFDLENBQUM7UUFDbkYsdUJBQWtCLEdBQWEsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQztRQUU3RCxrQkFBYSxHQUFHLEVBQUUsQ0FBQztRQUNuQixjQUFTLEdBQUcsS0FBSyxDQUFDO1FBSWxCLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFFbkIsb0JBQWUsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUVyQixzQkFBaUIsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO1FBRXZDLGdCQUFXLEdBQWEsRUFBRSxDQUFDO0lBd0tyQyxDQUFDO0lBcktDLDJCQUFJLEdBQUosVUFBSyxNQUFtQixFQUFFLEVBTzRDLEVBQUUsUUFBbUI7WUFQakUscUZBTzRDLEVBTjFELHdCQUFTLEVBQ1QsOEJBQVksRUFDWixrQ0FBYyxFQUNkLDBDQUFrQixFQUNsQixzQ0FBZ0IsRUFDaEIsd0NBQWlCO1FBQzJDLG9EQUFrQixDQUFDO1FBQ3pGLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxTQUFTLENBQUMsQ0FBQztRQUM1QyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsWUFBWSxDQUFDLENBQUM7UUFDbEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLGNBQWMsQ0FBQyxDQUFDO1FBQ3RELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFLGtCQUFrQixDQUFDLENBQUM7UUFDOUQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztRQUUxRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDO1FBQ3BGLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1FBRXpCLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDdEIsQ0FBQztJQUVELDZCQUFNLEdBQU4sVUFBTyxNQUFtQjtRQUN4QixJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNwQixNQUFNLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUU1QyxJQUFJLENBQUMsWUFBWSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDbEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1lBQzNELE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBRXRDLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUNoRCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ25ELElBQUksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDakUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNyRSxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxZQUFZLEVBQUUsWUFBWSxDQUFDLENBQUM7WUFDdkQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsZUFBZSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQ3BELElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUU3QyxJQUFJLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDaEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUN2RCxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFFL0MsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7U0FDeEI7SUFDSCxDQUFDO0lBRUssOEJBQU8sR0FBYjs7Ozs0QkFDRSxxQkFBTSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDOzt3QkFBM0MsU0FBMkMsQ0FBQzs7Ozs7S0FDN0M7SUFFSyxnQ0FBUyxHQUFmLFVBQWdCLEtBQW9COzs7O2dCQUNsQyxRQUFRLEtBQUssQ0FBQyxJQUFJLEVBQUU7b0JBQ2xCLEtBQUssV0FBVzt3QkFDZCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUN6RyxLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7d0JBQ3ZCLE1BQU07b0JBQ1IsS0FBSyxTQUFTO3dCQUNaLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLGVBQWUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7d0JBQ3pHLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQzt3QkFDdkIsTUFBTTtvQkFDUixLQUFLLE9BQU87d0JBQ1YsSUFBSSxJQUFJLENBQUMsZUFBZSxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFOzRCQUMvRSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQzt5QkFDN0U7d0JBQ0QsTUFBTTtpQkFDVDtnQkFFRCxLQUFTLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDeEQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsNkJBQTZCLENBQUMsQ0FBQztvQkFDakYsSUFBSSxDQUFDLEtBQUssSUFBSSxDQUFDLGVBQWUsRUFBRTt3QkFDOUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsNkJBQTZCLENBQUMsQ0FBQztxQkFDL0U7aUJBQ0Y7Ozs7S0FDRjtJQUVLLDZCQUFNLEdBQVosVUFBYSxLQUFVO1FBQVYsa0NBQVU7Ozs7Ozt3QkFDckIsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDOzhCQUNpRCxFQUFkLFNBQUksQ0FBQyxTQUFTOzs7NkJBQWQsZUFBYzt3QkFBMUQsV0FBd0MsRUFBdkMsTUFBTSxjQUFFLGNBQThCLEVBQTlCLE1BQU0sbUJBQUcsVUFBQyxLQUFVLElBQUssWUFBSyxFQUFMLENBQUs7d0JBQzVDLFVBQVUsR0FBRyxLQUFLLENBQUM7d0JBRWpCLE9BQU8sR0FBRyxJQUFJLE1BQU0sQ0FBQyxHQUFHLEdBQUcsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDOzhCQUNhO3dCQUFoQyxxQkFBTSxNQUFNLENBQUMsS0FBSyxDQUFDOzt3QkFBcEIsTUFBQyxTQUFtQixDQUFDLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQzs7OzZCQUFqQyxlQUFpQzt3QkFBL0MsVUFBVTt3QkFDbkIsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFOzRCQUM1QixJQUFJLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLENBQUM7NEJBQ25DLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDOzRCQUVsQyxVQUFVLEdBQUcsSUFBSSxDQUFDO3lCQUNuQjs7O3dCQU5zQixJQUFpQzs7O3dCQVMxRCxJQUFJLFVBQVUsRUFBRTs0QkFDZCxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7NEJBQ25CLHdCQUFNO3lCQUNQOzs7d0JBaEJvRCxJQUFjOzs7Ozs7S0FrQnRFO0lBRU8sbUNBQVksR0FBcEIsVUFBcUIsYUFBcUI7UUFDeEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNwQixJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFFYSxrQ0FBVyxHQUF6QixVQUEwQixLQUFhOzs7O2dCQUNyQyxZQUFZLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUVoQyxJQUFJLENBQUMsWUFBWSxHQUFHLFVBQVUsQ0FBQzs7OztnQ0FDN0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztxQ0FDbkIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsRUFBMUIsd0JBQTBCO2dDQUM1QixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztnQ0FDM0IsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO2dDQUNuQixxQkFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQzs7Z0NBQXhCLFNBQXdCLENBQUM7Z0NBQ3pCLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQzs7O2dDQUNkLElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7b0NBQzdCLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztpQ0FDckI7Ozs7O3FCQUNGLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7Ozs7S0FDNUI7SUFFTyxrQ0FBVyxHQUFuQjtRQUNFLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBRU8sa0NBQVcsR0FBbkI7UUFDRSxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUN2QixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUVPLHFDQUFjLEdBQXRCLFVBQXVCLEtBQWE7UUFDbEMsT0FBTyxLQUFLLEtBQUssSUFBSSxDQUFDLGFBQWEsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztJQUMxRCxDQUFDO0lBRU8sbUNBQVksR0FBcEI7UUFDRSxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztRQUN0QixJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7UUFDL0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO0lBQy9ELENBQUM7SUFFTyxrQ0FBVyxHQUFuQjtRQUNFLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsQ0FBQztJQUNsRSxDQUFDO0lBRU8sd0NBQWlCLEdBQXpCLFVBQTBCLFVBQWtCO1FBQTVDLGlCQU1DOztRQUxDLElBQU0sYUFBYSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDcEQsYUFBYSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDL0QsbUJBQWEsQ0FBQyxTQUFTLEVBQUMsR0FBRyxXQUFJLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtRQUN4RCxhQUFhLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLGNBQU0sWUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsRUFBN0IsQ0FBNkIsQ0FBQyxDQUFDO1FBQzdFLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQzdDLENBQUM7SUFFTyxrQ0FBVyxHQUFuQixVQUFvQixHQUFRLEVBQUUsU0FBaUI7UUFDN0MsSUFBSSxHQUFHLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsRUFBRTtZQUNyQyxHQUFHLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUNqQzthQUFNO1lBQ0wsR0FBRyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDOUI7SUFDSCxDQUFDO0lBRU8sa0NBQVcsR0FBbkIsVUFBb0IsR0FBUSxFQUFFLE1BQWdCOztRQUM1QyxTQUFHLENBQUMsU0FBUyxFQUFDLEdBQUcsV0FBSSxNQUFNLENBQUMsTUFBTSxDQUFDLGFBQUcsSUFBSSxVQUFHLENBQUMsTUFBTSxFQUFWLENBQVUsQ0FBQyxFQUFFO0lBQ3pELENBQUM7SUFFTyxrQ0FBVyxHQUFuQixVQUFvQixNQUFhLEVBQUUsS0FBaUI7UUFBakIsa0NBQWlCO1FBQ2xELE1BQU0sQ0FBQyxJQUFJLE9BQVgsTUFBTSxFQUFTLEtBQUssRUFBRTtRQUN0QixPQUFPLE1BQU0sQ0FBQztJQUNoQixDQUFDO0lBQ0gsbUJBQUM7QUFBRCxDQUFDO0FBcE1ZLG9DQUFZOzs7Ozs7Ozs7Ozs7Ozs7QUNMekIsd0ZBQTRDO0FBRTVDLENBQUM7SUFDRSxNQUFjLENBQUMsY0FBYyxDQUFDLEdBQUcsVUFBQyxNQUFXLEVBQUUsSUFBVSxFQUFFLEVBQVE7UUFDbEUsSUFBTSxFQUFFLEdBQUcsSUFBSSwyQkFBWSxFQUFFLENBQUM7UUFDOUIsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBRTFCLE9BQU8sRUFBRSxDQUFDO0lBQ1osQ0FBQztBQUNILENBQUMsQ0FBQyxFQUFFLENBQUM7Ozs7Ozs7Ozs7OztBQ1RMLHVDIiwiZmlsZSI6ImJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL2luZGV4LnRzXCIpO1xuIiwiaW1wb3J0IHtDYWxsYmFja0ludGVyZmFjZX0gZnJvbSAnLi9jYWxsYmFjay5pbnRlcmZhY2UnO1xuXG5leHBvcnQgaW50ZXJmYWNlIEF1dG9jb21wbGV0ZU9wdGlvbnNJbnRlcmZhY2Uge1xuICBjYWxsYmFja3M6IENhbGxiYWNrSW50ZXJmYWNlW10sXG4gIGlucHV0Q2xhc3Nlcz86IHN0cmluZ1tdLFxuICByZXN1bHRzQ2xhc3Nlcz86IHN0cmluZ1tdLFxuICByZXN1bHRzSXRlbUNsYXNzZXM/OiBzdHJpbmdbXSxcbiAgY29udGFpbmVyQ2xhc3Nlcz86IHN0cmluZ1tdLFxuICBpbnB1dFRpbWVvdXREZWxheT86IG51bWJlclxufVxuXG5leHBvcnQgY29uc3QgZGVmYXVsdEF1dG9jb21wbGV0ZU9wdGlvbnMgPSB7XG4gIGNhbGxiYWNrczogW10sXG4gIGlucHV0Q2xhc3NlczogW10sXG4gIHJlc3VsdHNDbGFzc2VzOiBbXSxcbiAgcmVzdWx0c0l0ZW1DbGFzc2VzOiBbXSxcbiAgY29udGFpbmVyQ2xhc3NlczogW10sXG4gIGlucHV0VGltZW91dERlbGF5OiA1MDBcbn07XG4iLCJpbXBvcnQge0NhbGxiYWNrSW50ZXJmYWNlfSBmcm9tICcuL2NhbGxiYWNrLmludGVyZmFjZSc7XG5pbXBvcnQge0F1dG9jb21wbGV0ZU9wdGlvbnNJbnRlcmZhY2UsIGRlZmF1bHRBdXRvY29tcGxldGVPcHRpb25zfSBmcm9tICcuL2F1dG9jb21wbGV0ZS1vcHRpb25zLmludGVyZmFjZSc7XG5cbmNvbnN0IHN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlL2F1dG9jb21wbGV0ZS5zY3NzJyk7XG5cbmV4cG9ydCBjbGFzcyBBdXRvQ29tcGxldGUge1xuICByZWFkb25seSBDU1NfUEFSRU5UX0NMQVNTID0gJ2NtLWFjJztcbiAgcmVhZG9ubHkgQ1NTX0xPQURJTkdfQ0xBU1MgPSAnY20tYWNfX2xvYWRpbmcnO1xuICByZWFkb25seSBDU1NfQ09OVEFJTkVSX0NMQVNTID0gJ2NtLWFjLWNvbnRhaW5lcic7XG4gIHJlYWRvbmx5IENTU19JTlBVVF9DTEFTUyA9ICdjbS1hYy1pbnB1dCc7XG4gIHJlYWRvbmx5IENTU19SRVNVTFRTX0NMQVNTID0gJ2NtLWFjLXJlc3VsdHMnO1xuICByZWFkb25seSBDU1NfUkVTVUxUU19ISURERU5fQ0xBU1MgPSB0aGlzLkNTU19SRVNVTFRTX0NMQVNTICsgJ19faGlkZGVuJztcbiAgcmVhZG9ubHkgQ1NTX1JFU1VMVFNfSVRFTV9DTEFTUyA9IHRoaXMuQ1NTX1JFU1VMVFNfQ0xBU1MgKyAnLS1pdGVtJztcbiAgcmVhZG9ubHkgQ1NTX1JFU1VMVFNfSVRFTV9BQ1RJVkVfQ0xBU1MgPSB0aGlzLkNTU19SRVNVTFRTX0lURU1fQ0xBU1MgKyAnX19hY3RpdmUnO1xuICByZWFkb25seSBJTlBVVF9USU1FT1VUID0gNTAwO1xuXG4gIHByaXZhdGUgY2FsbGJhY2tzOiBDYWxsYmFja0ludGVyZmFjZVtdID0gW107XG4gIHByaXZhdGUgaW5wdXRDbGFzc2VzOiBzdHJpbmdbXSA9IFt0aGlzLkNTU19JTlBVVF9DTEFTU107XG4gIHByaXZhdGUgY29udGFpbmVyQ2xhc3Nlczogc3RyaW5nW10gPSBbdGhpcy5DU1NfQ09OVEFJTkVSX0NMQVNTXTtcbiAgcHJpdmF0ZSByZXN1bHRzQ2xhc3Nlczogc3RyaW5nW10gPSBbdGhpcy5DU1NfUkVTVUxUU19DTEFTUywgdGhpcy5DU1NfUkVTVUxUU19ISURERU5fQ0xBU1NdO1xuICBwcml2YXRlIHJlc3VsdHNJdGVtQ2xhc3Nlczogc3RyaW5nW10gPSBbdGhpcy5DU1NfUkVTVUxUU19JVEVNX0NMQVNTXTtcblxuICBwcml2YXRlIHByZXZpb3VzSW5wdXQgPSAnJztcbiAgcHJpdmF0ZSBpc0xvYWRpbmcgPSBmYWxzZTtcbiAgcHJpdmF0ZSBkb21Db250YWluZXI6IGFueTtcbiAgcHJpdmF0ZSBkb21SZXN1bHRzOiBhbnk7XG4gIHByaXZhdGUgZG9tSW5wdXQ6IGFueTtcbiAgcHJpdmF0ZSBkb21DcmVhdGVkID0gZmFsc2U7XG5cbiAgcHJpdmF0ZSBjdXJyZW50Rm9jdXNJZHggPSAtMTtcbiAgcHJpdmF0ZSBpbnB1dFRpbWVvdXQ6IGFueTtcbiAgcHJpdmF0ZSBpbnB1dFRpbWVvdXREZWxheSA9IHRoaXMuSU5QVVRfVElNRU9VVDtcblxuICBwcml2YXRlIHN1Z2dlc3Rpb25zOiBzdHJpbmdbXSA9IFtdO1xuICBwcml2YXRlIHNlbGVjdENiOiBhbnk7XG5cbiAgaW5pdChwYXJlbnQ6IEhUTUxFbGVtZW50LCB7XG4gICAgICAgICAgICAgIGNhbGxiYWNrcyxcbiAgICAgICAgICAgICAgaW5wdXRDbGFzc2VzLFxuICAgICAgICAgICAgICByZXN1bHRzQ2xhc3NlcyxcbiAgICAgICAgICAgICAgcmVzdWx0c0l0ZW1DbGFzc2VzLFxuICAgICAgICAgICAgICBjb250YWluZXJDbGFzc2VzLFxuICAgICAgICAgICAgICBpbnB1dFRpbWVvdXREZWxheVxuICAgICAgICAgICAgfTogQXV0b2NvbXBsZXRlT3B0aW9uc0ludGVyZmFjZSA9IGRlZmF1bHRBdXRvY29tcGxldGVPcHRpb25zLCBzZWxlY3RDYiA9ICgpID0+IHt9KSB7XG4gICAgdGhpcy5hcHBlbmRBcnJheSh0aGlzLmNhbGxiYWNrcywgY2FsbGJhY2tzKTtcbiAgICB0aGlzLmFwcGVuZEFycmF5KHRoaXMuaW5wdXRDbGFzc2VzLCBpbnB1dENsYXNzZXMpO1xuICAgIHRoaXMuYXBwZW5kQXJyYXkodGhpcy5yZXN1bHRzQ2xhc3NlcywgcmVzdWx0c0NsYXNzZXMpO1xuICAgIHRoaXMuYXBwZW5kQXJyYXkodGhpcy5yZXN1bHRzSXRlbUNsYXNzZXMsIHJlc3VsdHNJdGVtQ2xhc3Nlcyk7XG4gICAgdGhpcy5hcHBlbmRBcnJheSh0aGlzLmNvbnRhaW5lckNsYXNzZXMsIGNvbnRhaW5lckNsYXNzZXMpO1xuXG4gICAgdGhpcy5pbnB1dFRpbWVvdXREZWxheSA9IGlucHV0VGltZW91dERlbGF5ID8gaW5wdXRUaW1lb3V0RGVsYXkgOiB0aGlzLklOUFVUX1RJTUVPVVQ7XG4gICAgdGhpcy5zZWxlY3RDYiA9IHNlbGVjdENiO1xuXG4gICAgdGhpcy5jcmVhdGUocGFyZW50KTtcbiAgfVxuXG4gIGNyZWF0ZShwYXJlbnQ6IEhUTUxFbGVtZW50KSB7XG4gICAgaWYgKCF0aGlzLmRvbUNyZWF0ZWQpIHtcbiAgICAgIHBhcmVudC5jbGFzc0xpc3QuYWRkKHRoaXMuQ1NTX1BBUkVOVF9DTEFTUyk7XG5cbiAgICAgIHRoaXMuZG9tQ29udGFpbmVyID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgICB0aGlzLmFwcGx5U3R5bGVzKHRoaXMuZG9tQ29udGFpbmVyLCB0aGlzLmNvbnRhaW5lckNsYXNzZXMpO1xuICAgICAgcGFyZW50LmFwcGVuZENoaWxkKHRoaXMuZG9tQ29udGFpbmVyKTtcblxuICAgICAgdGhpcy5kb21JbnB1dCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2lucHV0Jyk7XG4gICAgICB0aGlzLmFwcGx5U3R5bGVzKHRoaXMuZG9tSW5wdXQsIHRoaXMuaW5wdXRDbGFzc2VzKTtcbiAgICAgIHRoaXMuZG9tSW5wdXQuYWRkRXZlbnRMaXN0ZW5lcigna2V5dXAnLCB0aGlzLm9uS2V5VXAuYmluZCh0aGlzKSk7XG4gICAgICB0aGlzLmRvbUlucHV0LmFkZEV2ZW50TGlzdGVuZXIoJ2tleWRvd24nLCB0aGlzLm9uS2V5RG93bi5iaW5kKHRoaXMpKTtcbiAgICAgIHRoaXMuZG9tSW5wdXQuc2V0QXR0cmlidXRlKCdhcmlhLWxhYmVsJywgJ1Rlc3QgbGFiZWwnKTtcbiAgICAgIHRoaXMuZG9tSW5wdXQuc2V0QXR0cmlidXRlKCdhcmlhLXJlcXVpcmVkJywgJ3RydWUnKTtcbiAgICAgIHRoaXMuZG9tQ29udGFpbmVyLmFwcGVuZENoaWxkKHRoaXMuZG9tSW5wdXQpO1xuXG4gICAgICB0aGlzLmRvbVJlc3VsdHMgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICAgIHRoaXMuYXBwbHlTdHlsZXModGhpcy5kb21SZXN1bHRzLCB0aGlzLnJlc3VsdHNDbGFzc2VzKTtcbiAgICAgIHRoaXMuZG9tQ29udGFpbmVyLmFwcGVuZENoaWxkKHRoaXMuZG9tUmVzdWx0cyk7XG5cbiAgICAgIHRoaXMuZG9tQ3JlYXRlZCA9IHRydWU7XG4gICAgfVxuICB9XG5cbiAgYXN5bmMgb25LZXlVcCgpIHtcbiAgICBhd2FpdCB0aGlzLmhhbmRsZUlucHV0KHRoaXMuZG9tSW5wdXQudmFsdWUpO1xuICB9XG5cbiAgYXN5bmMgb25LZXlEb3duKGV2ZW50OiBLZXlib2FyZEV2ZW50KSB7XG4gICAgc3dpdGNoIChldmVudC5jb2RlKSB7XG4gICAgICBjYXNlIFwiQXJyb3dEb3duXCI6XG4gICAgICAgIHRoaXMuY3VycmVudEZvY3VzSWR4ID0gdGhpcy5jdXJyZW50Rm9jdXNJZHggPCB0aGlzLnN1Z2dlc3Rpb25zLmxlbmd0aCAtIDEgPyB0aGlzLmN1cnJlbnRGb2N1c0lkeCArIDEgOiAwO1xuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgXCJBcnJvd1VwXCI6XG4gICAgICAgIHRoaXMuY3VycmVudEZvY3VzSWR4ID0gdGhpcy5jdXJyZW50Rm9jdXNJZHggPiAwID8gdGhpcy5jdXJyZW50Rm9jdXNJZHggLSAxIDogdGhpcy5zdWdnZXN0aW9ucy5sZW5ndGggLSAxO1xuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgXCJFbnRlclwiOlxuICAgICAgICBpZiAodGhpcy5jdXJyZW50Rm9jdXNJZHggPj0gMCAmJiB0aGlzLmN1cnJlbnRGb2N1c0lkeCA8IHRoaXMuc3VnZ2VzdGlvbnMubGVuZ3RoKSB7XG4gICAgICAgICAgdGhpcy5zZWxlY3RSZXN1bHQodGhpcy5kb21SZXN1bHRzLmNoaWxkcmVuW3RoaXMuY3VycmVudEZvY3VzSWR4XS5pbm5lckhUTUwpO1xuICAgICAgICB9XG4gICAgICAgIGJyZWFrO1xuICAgIH1cblxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5kb21SZXN1bHRzLmNoaWxkcmVuLmxlbmd0aDsgaSsrKSB7XG4gICAgICB0aGlzLmRvbVJlc3VsdHMuY2hpbGRyZW5baV0uY2xhc3NMaXN0LnJlbW92ZSh0aGlzLkNTU19SRVNVTFRTX0lURU1fQUNUSVZFX0NMQVNTKTtcbiAgICAgIGlmIChpID09PSB0aGlzLmN1cnJlbnRGb2N1c0lkeCkge1xuICAgICAgICB0aGlzLmRvbVJlc3VsdHMuY2hpbGRyZW5baV0uY2xhc3NMaXN0LmFkZCh0aGlzLkNTU19SRVNVTFRTX0lURU1fQUNUSVZFX0NMQVNTKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBhc3luYyB1cGRhdGUoaW5wdXQgPSAnJykge1xuICAgIHRoaXMuY2xlYXJSZXN1bHRzKCk7XG4gICAgZm9yIChjb25zdCB7c291cmNlLCByZW5kZXIgPSAodmFsdWU6IGFueSkgPT4gdmFsdWV9IG9mIHRoaXMuY2FsbGJhY2tzKSB7XG4gICAgICBsZXQgZm91bmRNYXRjaCA9IGZhbHNlO1xuXG4gICAgICBjb25zdCBwYXR0ZXJuID0gbmV3IFJlZ0V4cChcIl5cIiArIGlucHV0LCBcImlcIik7XG4gICAgICBmb3IgKGNvbnN0IHN1Z2dlc3Rpb24gb2YgKGF3YWl0IHNvdXJjZShpbnB1dCkpLm1hcChyZW5kZXIpKSB7XG4gICAgICAgIGlmIChwYXR0ZXJuLnRlc3Qoc3VnZ2VzdGlvbikpIHtcbiAgICAgICAgICB0aGlzLnJlbmRlclJlc3VsdHNJdGVtKHN1Z2dlc3Rpb24pO1xuICAgICAgICAgIHRoaXMuc3VnZ2VzdGlvbnMucHVzaChzdWdnZXN0aW9uKTtcblxuICAgICAgICAgIGZvdW5kTWF0Y2ggPSB0cnVlO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmIChmb3VuZE1hdGNoKSB7XG4gICAgICAgIHRoaXMuc2hvd1Jlc3VsdHMoKTtcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBzZWxlY3RSZXN1bHQoc2VsZWN0ZWRWYWx1ZTogc3RyaW5nKSB7XG4gICAgdGhpcy5kb21JbnB1dC52YWx1ZSA9IHNlbGVjdGVkVmFsdWU7XG4gICAgdGhpcy5jbGVhclJlc3VsdHMoKTtcbiAgICB0aGlzLnNlbGVjdENiKHNlbGVjdGVkVmFsdWUpO1xuICB9XG5cbiAgcHJpdmF0ZSBhc3luYyBoYW5kbGVJbnB1dChpbnB1dDogc3RyaW5nKSB7XG4gICAgY2xlYXJUaW1lb3V0KHRoaXMuaW5wdXRUaW1lb3V0KTtcblxuICAgIHRoaXMuaW5wdXRUaW1lb3V0ID0gc2V0VGltZW91dChhc3luYyAoKSA9PiB7XG4gICAgICBjb25zb2xlLmxvZygnVXBkYWluZycpO1xuICAgICAgaWYgKHRoaXMuaXNVcGRhdGVOZWVkZWQoaW5wdXQpKSB7XG4gICAgICAgIHRoaXMucHJldmlvdXNJbnB1dCA9IGlucHV0O1xuICAgICAgICB0aGlzLnNob3dMb2FkaW5nKCk7XG4gICAgICAgIGF3YWl0IHRoaXMudXBkYXRlKGlucHV0KTtcbiAgICAgICAgdGhpcy5oaWRlTG9hZGluZygpO1xuICAgICAgfSBlbHNlIGlmIChpbnB1dC5sZW5ndGggPT09IDApIHtcbiAgICAgICAgdGhpcy5jbGVhclJlc3VsdHMoKTtcbiAgICAgIH1cbiAgICB9LCB0aGlzLmlucHV0VGltZW91dERlbGF5KTtcbiAgfVxuXG4gIHByaXZhdGUgc2hvd0xvYWRpbmcoKTogdm9pZCB7XG4gICAgdGhpcy5pc0xvYWRpbmcgPSB0cnVlO1xuICAgIHRoaXMudG9nZ2xlQ2xhc3ModGhpcy5kb21Db250YWluZXIsIHRoaXMuQ1NTX0xPQURJTkdfQ0xBU1MpO1xuICB9XG5cbiAgcHJpdmF0ZSBoaWRlTG9hZGluZygpOiB2b2lkIHtcbiAgICB0aGlzLmlzTG9hZGluZyA9IGZhbHNlO1xuICAgIHRoaXMudG9nZ2xlQ2xhc3ModGhpcy5kb21Db250YWluZXIsIHRoaXMuQ1NTX0xPQURJTkdfQ0xBU1MpO1xuICB9XG5cbiAgcHJpdmF0ZSBpc1VwZGF0ZU5lZWRlZChpbnB1dDogc3RyaW5nKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIGlucHV0ICE9PSB0aGlzLnByZXZpb3VzSW5wdXQgJiYgaW5wdXQubGVuZ3RoID4gMDtcbiAgfVxuXG4gIHByaXZhdGUgY2xlYXJSZXN1bHRzKCk6IHZvaWQge1xuICAgIHRoaXMuc3VnZ2VzdGlvbnMgPSBbXTtcbiAgICB0aGlzLmRvbVJlc3VsdHMuaW5uZXJIVE1MID0gJyc7XG4gICAgdGhpcy5kb21SZXN1bHRzLmNsYXNzTGlzdC5hZGQodGhpcy5DU1NfUkVTVUxUU19ISURERU5fQ0xBU1MpO1xuICB9XG5cbiAgcHJpdmF0ZSBzaG93UmVzdWx0cygpOiB2b2lkIHtcbiAgICB0aGlzLmRvbVJlc3VsdHMuY2xhc3NMaXN0LnJlbW92ZSh0aGlzLkNTU19SRVNVTFRTX0hJRERFTl9DTEFTUyk7XG4gIH1cblxuICBwcml2YXRlIHJlbmRlclJlc3VsdHNJdGVtKHN1Z2dlc3Rpb246IHN0cmluZyk6IHZvaWQge1xuICAgIGNvbnN0IGRvbVN1Z2dlc3Rpb24gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICBkb21TdWdnZXN0aW9uLmFwcGVuZENoaWxkKGRvY3VtZW50LmNyZWF0ZVRleHROb2RlKHN1Z2dlc3Rpb24pKTtcbiAgICBkb21TdWdnZXN0aW9uLmNsYXNzTGlzdC5hZGQoLi4udGhpcy5yZXN1bHRzSXRlbUNsYXNzZXMpO1xuICAgIGRvbVN1Z2dlc3Rpb24uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoKSA9PiB0aGlzLnNlbGVjdFJlc3VsdChzdWdnZXN0aW9uKSk7XG4gICAgdGhpcy5kb21SZXN1bHRzLmFwcGVuZENoaWxkKGRvbVN1Z2dlc3Rpb24pO1xuICB9XG5cbiAgcHJpdmF0ZSB0b2dnbGVDbGFzcyhkb206IGFueSwgY2xhc3NOYW1lOiBzdHJpbmcpIHtcbiAgICBpZiAoZG9tLmNsYXNzTGlzdC5jb250YWlucyhjbGFzc05hbWUpKSB7XG4gICAgICBkb20uY2xhc3NMaXN0LnJlbW92ZShjbGFzc05hbWUpO1xuICAgIH0gZWxzZSB7XG4gICAgICBkb20uY2xhc3NMaXN0LmFkZChjbGFzc05hbWUpO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgYXBwbHlTdHlsZXMoZG9tOiBhbnksIHN0eWxlczogc3RyaW5nW10pIHtcbiAgICBkb20uY2xhc3NMaXN0LmFkZCguLi5zdHlsZXMuZmlsdGVyKGNzcyA9PiBjc3MubGVuZ3RoKSk7XG4gIH1cblxuICBwcml2YXRlIGFwcGVuZEFycmF5KHNvdXJjZTogYW55W10sIGFycmF5OiBhbnlbXSA9IFtdKSB7XG4gICAgc291cmNlLnB1c2goLi4uYXJyYXkpO1xuICAgIHJldHVybiBzb3VyY2U7XG4gIH1cbn1cbiIsImltcG9ydCB7QXV0b0NvbXBsZXRlfSBmcm9tICcuL2F1dG9jb21wbGV0ZSc7XG5cbihmdW5jdGlvbigpe1xuICAod2luZG93IGFzIGFueSlbJ2F1dG9Db21wbGV0ZSddID0gKHBhcmVudDogYW55LCBvcHRzPzogYW55LCBjYj86IGFueSkgPT4ge1xuICAgIGNvbnN0IGFjID0gbmV3IEF1dG9Db21wbGV0ZSgpO1xuICAgIGFjLmluaXQocGFyZW50LCBvcHRzLCBjYik7XG5cbiAgICByZXR1cm4gYWM7XG4gIH1cbn0pKCk7XG4iLCIvLyBleHRyYWN0ZWQgYnkgbWluaS1jc3MtZXh0cmFjdC1wbHVnaW4iXSwic291cmNlUm9vdCI6IiJ9
