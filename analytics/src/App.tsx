import React, {useState} from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import './App.css';
import ACComponent from './component/AutoComplete';
import {ChartComponent} from './component/Chart';

const App: React.FC = () => {
    const [selectedCity, setSelectedCity] = useState('');

    return (
        <div className="App">
            <div className="wrapper">
                <header className="App-header">
                    <Navbar bg="light" expand="lg">
                        <Navbar.Brand href="#home">ClearMove</Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                    </Navbar>
                </header>
                <Container>
                    <Row>
                        <Col><ACComponent onCityChange={setSelectedCity}/></Col>
                    </Row>
                    <ChartComponent selectedCity={selectedCity}/>
                </Container>
                <div className="push"/>
            </div>
            <footer className="footer">
                <div className="container">
                    <span className="text-muted">ClearMove</span>
                </div>
            </footer>
        </div>
    );
}

export default App;
