import React, {useEffect, useState} from 'react';
import {Chart} from 'react-google-charts';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import axios from 'axios';

import {CityDataInterface} from '../interface/city-data.interface';

export const ChartComponent = (props: { selectedCity: string }) => {
    const [population, setPopulation] = useState<number[][]>([[0, 0]]);
    const [costOfLiving, setCostOfLiving] = useState<number[][]>([[0, 0]]);

    useEffect(() => {
        if (props.selectedCity) {
            axios.get<CityDataInterface[]>(`fixtures/data/${props.selectedCity}.json`).then(
                cityData => {
                    setPopulation(cityData.data.map(({year, population}) => [year, population]));
                    setCostOfLiving(cityData.data.map(({year, cost}) => [year, cost]));
                }
            );
        }
    }, [props.selectedCity]);

    if (props.selectedCity) {
        return (
            <Row>
                <Col lg={true}>
                    <h1>Population in <b>{props.selectedCity}</b></h1>
                    <Chart
                        chartType="LineChart"
                        data={[['Year', 'Population'], ...population]}
                        width="100%"
                        height="400px"
                        legendToggle
                    />
                </Col>
                <Col lg={true}>
                    <h1>Cost of living in <b>{props.selectedCity}</b></h1>
                    <Chart
                        chartType="LineChart"
                        data={[['Year', 'Cost of living'], ...costOfLiving]}
                        width="100%"
                        height="400px"
                        legendToggle
                    />
                </Col>
            </Row>
        )
    }

    return <Row><Col>Please select city to its its population and cost of living charts</Col></Row>;
};
