import React from 'react';
import axios from 'axios';

import {CityInterface} from '../interface/city.interface';

class ACComponent extends React.Component<{onCityChange: Function}, {}> {
    private el: HTMLDivElement | null = null;

    componentDidMount() {
        axios.get<CityInterface[]>(`fixtures/cities.json`).then(
            cityData => {
                const callbacks = [{ source: async () => cityData.data.map(i => i.city) }];
                (window as any).autoComplete(this.el as HTMLDivElement, { callbacks }, this.props.onCityChange);
            }
        );
    }

    componentWillUnmount() {
        (this.el as HTMLDivElement).innerHTML = '';
    }

    render() {
        return <div className="ACComponent">
            <label>Type in city name:</label>
            <div ref={el => this.el = el} />
        </div>
    }
}

export default ACComponent;
