export interface CityDataInterface {
    year: number;
    population: number;
    cost: number;
}
