import {SourceCallbackInterface} from './source-callback.interface';
import {RenderCallbackInterface} from './render-callback.interface';

export interface CallbackInterface {
  source: SourceCallbackInterface;
  render?: RenderCallbackInterface;
}
