export interface RenderCallbackInterface {
  (source: string): string;
}
