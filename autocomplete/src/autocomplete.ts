import {CallbackInterface} from './callback.interface';
import {AutocompleteOptionsInterface, defaultAutocompleteOptions} from './autocomplete-options.interface';

const styles = require('../style/autocomplete.scss');

export class AutoComplete {
  readonly CSS_PARENT_CLASS = 'cm-ac';
  readonly CSS_LOADING_CLASS = 'cm-ac__loading';
  readonly CSS_CONTAINER_CLASS = 'cm-ac-container';
  readonly CSS_INPUT_CLASS = 'cm-ac-input';
  readonly CSS_RESULTS_CLASS = 'cm-ac-results';
  readonly CSS_RESULTS_HIDDEN_CLASS = this.CSS_RESULTS_CLASS + '__hidden';
  readonly CSS_RESULTS_ITEM_CLASS = this.CSS_RESULTS_CLASS + '--item';
  readonly CSS_RESULTS_ITEM_ACTIVE_CLASS = this.CSS_RESULTS_ITEM_CLASS + '__active';
  readonly INPUT_TIMEOUT = 500;

  private callbacks: CallbackInterface[] = [];
  private inputClasses: string[] = [this.CSS_INPUT_CLASS];
  private containerClasses: string[] = [this.CSS_CONTAINER_CLASS];
  private resultsClasses: string[] = [this.CSS_RESULTS_CLASS, this.CSS_RESULTS_HIDDEN_CLASS];
  private resultsItemClasses: string[] = [this.CSS_RESULTS_ITEM_CLASS];

  private previousInput = '';
  private isLoading = false;
  private domContainer: any;
  private domResults: any;
  private domInput: any;
  private domCreated = false;

  private currentFocusIdx = -1;
  private inputTimeout: any;
  private inputTimeoutDelay = this.INPUT_TIMEOUT;

  private suggestions: string[] = [];
  private selectCb: any;

  init(parent: HTMLElement, {
              callbacks,
              inputClasses,
              resultsClasses,
              resultsItemClasses,
              containerClasses,
              inputTimeoutDelay
            }: AutocompleteOptionsInterface = defaultAutocompleteOptions, selectCb = () => {}) {
    this.appendArray(this.callbacks, callbacks);
    this.appendArray(this.inputClasses, inputClasses);
    this.appendArray(this.resultsClasses, resultsClasses);
    this.appendArray(this.resultsItemClasses, resultsItemClasses);
    this.appendArray(this.containerClasses, containerClasses);

    this.inputTimeoutDelay = inputTimeoutDelay ? inputTimeoutDelay : this.INPUT_TIMEOUT;
    this.selectCb = selectCb;

    this.create(parent);
  }

  create(parent: HTMLElement) {
    if (!this.domCreated) {
      parent.classList.add(this.CSS_PARENT_CLASS);

      this.domContainer = document.createElement('div');
      this.applyStyles(this.domContainer, this.containerClasses);
      parent.appendChild(this.domContainer);

      this.domInput = document.createElement('input');
      this.applyStyles(this.domInput, this.inputClasses);
      this.domInput.addEventListener('keyup', this.onKeyUp.bind(this));
      this.domInput.addEventListener('keydown', this.onKeyDown.bind(this));
      this.domInput.setAttribute('aria-label', 'Test label');
      this.domInput.setAttribute('aria-required', 'true');
      this.domContainer.appendChild(this.domInput);

      this.domResults = document.createElement('div');
      this.applyStyles(this.domResults, this.resultsClasses);
      this.domContainer.appendChild(this.domResults);

      this.domCreated = true;
    }
  }

  async onKeyUp() {
    await this.handleInput(this.domInput.value);
  }

  async onKeyDown(event: KeyboardEvent) {
    switch (event.code) {
      case "ArrowDown":
        this.currentFocusIdx = this.currentFocusIdx < this.suggestions.length - 1 ? this.currentFocusIdx + 1 : 0;
        event.preventDefault();
        break;
      case "ArrowUp":
        this.currentFocusIdx = this.currentFocusIdx > 0 ? this.currentFocusIdx - 1 : this.suggestions.length - 1;
        event.preventDefault();
        break;
      case "Enter":
        if (this.currentFocusIdx >= 0 && this.currentFocusIdx < this.suggestions.length) {
          this.selectResult(this.domResults.children[this.currentFocusIdx].innerHTML);
        }
        break;
    }

    for (let i = 0; i < this.domResults.children.length; i++) {
      this.domResults.children[i].classList.remove(this.CSS_RESULTS_ITEM_ACTIVE_CLASS);
      if (i === this.currentFocusIdx) {
        this.domResults.children[i].classList.add(this.CSS_RESULTS_ITEM_ACTIVE_CLASS);
      }
    }
  }

  async update(input = '') {
    this.clearResults();
    for (const {source, render = (value: any) => value} of this.callbacks) {
      let foundMatch = false;

      const pattern = new RegExp("^" + input, "i");
      for (const suggestion of (await source(input)).map(render)) {
        if (pattern.test(suggestion)) {
          this.renderResultsItem(suggestion);
          this.suggestions.push(suggestion);

          foundMatch = true;
        }
      }

      if (foundMatch) {
        this.showResults();
        break;
      }
    }
  }

  private selectResult(selectedValue: string) {
    this.domInput.value = selectedValue;
    this.clearResults();
    this.selectCb(selectedValue);
  }

  private async handleInput(input: string) {
    clearTimeout(this.inputTimeout);

    this.inputTimeout = setTimeout(async () => {
      console.log('Updaing');
      if (this.isUpdateNeeded(input)) {
        this.previousInput = input;
        this.showLoading();
        await this.update(input);
        this.hideLoading();
      } else if (input.length === 0) {
        this.clearResults();
      }
    }, this.inputTimeoutDelay);
  }

  private showLoading(): void {
    this.isLoading = true;
    this.toggleClass(this.domContainer, this.CSS_LOADING_CLASS);
  }

  private hideLoading(): void {
    this.isLoading = false;
    this.toggleClass(this.domContainer, this.CSS_LOADING_CLASS);
  }

  private isUpdateNeeded(input: string): boolean {
    return input !== this.previousInput && input.length > 0;
  }

  private clearResults(): void {
    this.suggestions = [];
    this.domResults.innerHTML = '';
    this.domResults.classList.add(this.CSS_RESULTS_HIDDEN_CLASS);
  }

  private showResults(): void {
    this.domResults.classList.remove(this.CSS_RESULTS_HIDDEN_CLASS);
  }

  private renderResultsItem(suggestion: string): void {
    const domSuggestion = document.createElement('div');
    domSuggestion.appendChild(document.createTextNode(suggestion));
    domSuggestion.classList.add(...this.resultsItemClasses);
    domSuggestion.addEventListener('click', () => this.selectResult(suggestion));
    this.domResults.appendChild(domSuggestion);
  }

  private toggleClass(dom: any, className: string) {
    if (dom.classList.contains(className)) {
      dom.classList.remove(className);
    } else {
      dom.classList.add(className);
    }
  }

  private applyStyles(dom: any, styles: string[]) {
    dom.classList.add(...styles.filter(css => css.length));
  }

  private appendArray(source: any[], array: any[] = []) {
    source.push(...array);
    return source;
  }
}
