import {CallbackInterface} from './callback.interface';

export interface AutocompleteOptionsInterface {
  callbacks: CallbackInterface[],
  inputClasses?: string[],
  resultsClasses?: string[],
  resultsItemClasses?: string[],
  containerClasses?: string[],
  inputTimeoutDelay?: number
}

export const defaultAutocompleteOptions = {
  callbacks: [],
  inputClasses: [],
  resultsClasses: [],
  resultsItemClasses: [],
  containerClasses: [],
  inputTimeoutDelay: 500
};
