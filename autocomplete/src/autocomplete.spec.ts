import {AutoComplete} from './autocomplete';
import {CallbackInterface} from './callback.interface';

describe('Autocomplete component', () => {
  let component: AutoComplete;
  let parentEl: HTMLElement;
  let inputEl: HTMLInputElement;
  let resultsEl: HTMLDivElement;
  let resultsItems: HTMLCollectionOf<any>;

  beforeEach(() => {
    document.body.innerHTML = '<div id="testAc"></div>';
    parentEl = document.getElementById('testAc') as HTMLElement;
    component = new AutoComplete();
  });

  it('should render container > input > results', () => {
    component.init(parentEl);
    expect(document.getElementsByClassName(component.CSS_CONTAINER_CLASS).length).toBe(1);
    expect(document.getElementsByClassName(component.CSS_RESULTS_CLASS).length).toBe(1);
    expect(document.getElementsByClassName(component.CSS_INPUT_CLASS).length).toBe(1);
  });

  it('should handle additional styles from options', () => {
    const callbacks: CallbackInterface[] = [];
    const inputClasses = ['my-input-class'];
    const resultsClasses = ['my-results-class'];
    const containerClasses = ['my-container-class'];

    component.init(parentEl, {
      callbacks,
      inputClasses,
      resultsClasses,
      containerClasses
    });

    expect(document.getElementsByClassName(inputClasses[0]).length).toBe(1);
    expect(document.getElementsByClassName(resultsClasses[0]).length).toBe(1);
    expect(document.getElementsByClassName(containerClasses[0]).length).toBe(1);
  });

  describe('Callbacks and callback chain', () => {
    it('should handle input and callbacks with render', async () => {
      const callbacks: CallbackInterface[] = [{
        source: (input) => ['apple', 'apples', 'ananas', 'banana', 'bag'],
        render: (input) => input + '#'
      }];
      const resultsItemClasses = ['my-results-item-class'];

      component.init(parentEl, {callbacks, resultsItemClasses });

      await component.update('a');

      const resultsItems = document.getElementsByClassName(component.CSS_RESULTS_ITEM_CLASS);

      expect(resultsItems.length).toBe(3);
      expect(document.getElementsByClassName(resultsItemClasses[0]).length).toBe(3);
      expect((resultsItems.item(0) as HTMLDivElement).innerHTML).toBe('apple#');
    });

    it('should render results from callbacks that return non empty data', async () => {
      const callbacks: CallbackInterface[] = [{
        source: (input) => ['apple', 'apples', 'ananas'],
        render: (input) => input + '#'
      }, {
        source: (input) => ['banana', 'bag'],
        render: (input) => input + '$$$'
      }];

      component.init(parentEl, {callbacks});

      await component.update('b');

      const resultsItems = document.getElementsByClassName(component.CSS_RESULTS_ITEM_CLASS);

      expect(resultsItems.length).toBe(2);
      expect((resultsItems.item(0) as HTMLDivElement).innerHTML).toBe('banana$$$');
    });
  });


  it('should handle result item selection', async () => {
    const callbacks: CallbackInterface[] = [{
      source: (input) => ['apple', 'apples', 'ananas'],
    }];

    component.init(parentEl, {callbacks});
    inputEl = (document.getElementsByClassName(component.CSS_INPUT_CLASS).item(0) as HTMLInputElement);
    resultsEl = (document.getElementsByClassName(component.CSS_RESULTS_CLASS).item(0) as HTMLDivElement);

    await component.update('an');
    const resultsItems = document.getElementsByClassName(component.CSS_RESULTS_ITEM_CLASS);
    (resultsItems.item(0) as HTMLDivElement).click();

    expect(inputEl.value).toBe('ananas');
  });

  describe('Keyboard events', () => {
    const initComponent = async (data = ['apple', 'apples', 'ananas'], inputValue = 'a') => {
      const callbacks: CallbackInterface[] = [{source: (input) => data}];

      component.init(parentEl, {callbacks});
      inputEl = (document.getElementsByClassName(component.CSS_INPUT_CLASS).item(0) as HTMLInputElement);
      resultsEl = (document.getElementsByClassName(component.CSS_RESULTS_CLASS).item(0) as HTMLDivElement);
      resultsItems = document.getElementsByClassName(component.CSS_RESULTS_ITEM_CLASS);

      inputEl.value = inputValue;
      await component.update(inputValue);
    };

    it('should handle keyboard arrow up', async () => {
      await initComponent();
      await component.onKeyDown({ code: "ArrowUp", preventDefault: () => {} } as KeyboardEvent);

      expect((resultsItems.item(resultsItems.length - 1) as HTMLDivElement).classList.contains(component.CSS_RESULTS_ITEM_ACTIVE_CLASS)).toBeTruthy();
    });

    it('should handle keyboard arrow down', async () => {
      await initComponent();
      await component.onKeyDown({ code: "ArrowDown", preventDefault: () => {} } as KeyboardEvent);

      expect((resultsItems.item(0) as HTMLDivElement).classList.contains(component.CSS_RESULTS_ITEM_ACTIVE_CLASS)).toBeTruthy();
    });

    it('should handle keyboard enter/selection', async () => {
      await initComponent();

      await component.onKeyDown({ code: "ArrowDown", preventDefault: () => {} } as KeyboardEvent);
      await component.onKeyDown({ code: "Enter", preventDefault: () => {} } as KeyboardEvent);

      expect(inputEl.value).toBe('apple');
    });

    it('should handle ingore keyboard enter/selection without selecting results', async () => {
      await initComponent();

      await component.onKeyDown({ code: "Enter", preventDefault: () => {} } as KeyboardEvent);

      expect(inputEl.value).toBe('a');
    });
  });
});
