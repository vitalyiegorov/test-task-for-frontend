export interface SourceCallbackInterface {
  (input: string): string[];
}
