import {AutoComplete} from './autocomplete';

(function(){
  (window as any)['autoComplete'] = (parent: any, opts?: any, cb?: any) => {
    const ac = new AutoComplete();
    ac.init(parent, opts, cb);

    return ac;
  }
})();
